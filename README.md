# Gane of Drones

## Install Database
In the repor you'll find uruit.sql which will work as dummy db

## Backend

In the backend folder run
````
npm install
````
Then config the backend as need in
````
backend/config.js
````
Launch api
````
node index
````


## Frontend

Remember to have launched the api in localhost:8080

In the frontend folder run
````
npm install
````
To run react app
````
npm run start
````
To run tests
````
npm run test
````
