const config = require('./config')
const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const sql = require('./sql');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', `*`);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/', (req, res) => res.send('not suposed to be here!'))

// Moves
app.get('/moves', function (req, res) {
    query = `select * from moves`;
    sql.query(query, (result)=> {
        res.json(result)
    });    
});

app.post('/moves', function (req, res) {
    const params = req.body;
    const move = params.move;
    const kills = params.kills;
    const query = `
        insert into moves 
            (move, kills) 
        values 
            ('${move}', '${kills}')`;
    sql.query(query, (result)=> {
        const response = { 
            id: result.insertId,
            move,
            kills
        };
    res.send(JSON.stringify(response))    //todo after insert
    });
    
});

app.delete('/moves/:moveId', function (req, res) {
    const { moveId } = req.params;
    const query = `DELETE FROM moves WHERE id=${moveId};`;
    sql.query(query, (result)=> {
        res.send(JSON.stringify({moveId: moveId}))    //todo after insert
    });
  })

// Scores
app.get('/scores', function (req, res) {
    query = `select 
	player_name,
	count(id) as matches,
	sum(win) as wins,
	count(id) - sum(win) as lost,
	sum(tries) as tries,
	sum(tries_win) as tries_win
    from scores group by player_name
    order by wins desc`;
    sql.query(query, (result)=> {
        res.json(result)
    });    
});
app.post('/scores', function (req, res) {
    const params = req.body;
    const names = params.names;
    const winner = params.player1Wins > params.player2Wins ? { player1: 1, player2: 0} : { player1: 0, player2: 1};
    const query = `
        insert into scores 
            (player_name, win, tries, tries_win) 
        values 
        ('${names.player1}', ${winner.player1}, ${params.tries}, ${params.player1Wins} ),
        ('${names.player2}', ${winner.player2}, ${params.tries}, ${params.player2Wins} )`;
    sql.query(query, (result)=> {
        //todo after insert
    });
    let response = { response: 'inserted'};
    res.send(JSON.stringify(response))
});

app.listen(config.api.port, () => console.log(`Example app listening on port ${config.api.port}!`))