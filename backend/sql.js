
const mysql = require('mysql');
const config = require('./config')

module.exports = {
    query: function(queryStr, callback = null ) {
        const con = mysql.createConnection(config.connectionConfig);
        let response = false;
        con.connect(function(err) {
            if (err) throw err;    
        });   
        con.query(queryStr, function (error, results, fields) {
            if (error) throw error;
            if (callback) {
                callback(results);
            }
        });
        con.end();
    }
}
