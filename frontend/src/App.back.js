import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import sigarObj from './sigarObj.json';
import { clone } from 'ramda';
import './App.css';
import logo from './images/logo.png';
import lupa from './images/magnifying-glass-3-24.png';

import * as actionsActivityLog from './export/actions/ActivityLogActions';
import * as actionsAdProduct from './export/actions/AdProductActions';
import * as actionsAdUnitTypeGroup from './export/actions/AdUnitTypeGroupActions';
import * as actionsAdUnitTypes from './export/actions/AdUnitTypesActions';
import * as actionsAd_Buys from './export/actions/Ad_BuysActions';
import * as actionsAdvertisers from './export/actions/AdvertisersActions';
import * as actionsBroswer_Family from './export/actions/Broswer_FamilyActions';
import * as actionsBroswer_Versions from './export/actions/Broswer_VersionsActions';
import * as actionsBrowser_Type from './export/actions/Browser_TypeActions';
import * as actionsBrowser_Type_Group from './export/actions/Browser_Type_GroupActions';
import * as actionsBusiness_Unit from './export/actions/Business_UnitActions';
import * as actionsCampaigns from './export/actions/CampaignsActions';
import * as actionsCity from './export/actions/CityActions';
import * as actionsCountry from './export/actions/CountryActions';
import * as actionsEmbedCodes from './export/actions/EmbedCodesActions';
import * as actionsExport_Ads_txt_Reseller_Entries from './export/actions/Export_Ads_txt_Reseller_EntriesActions';
import * as actionsImplementation_type from './export/actions/Implementation_typeActions';
import * as actionsIndustry from './export/actions/IndustryActions';
import * as actionsIndustryCategory from './export/actions/IndustryCategoryActions';
import * as actionsInfo from './export/actions/InfoActions';
import * as actionsMetro_Code from './export/actions/Metro_CodeActions';
import * as actionsOsFamilyName from './export/actions/OsFamilyNameActions';
import * as actionsPixel_Black_List from './export/actions/Pixel_Black_ListActions';
import * as actionsPublisher from './export/actions/PublisherActions';
import * as actionsPublisher_Payment_Info from './export/actions/Publisher_Payment_InfoActions';
import * as actionsPublisher_Payment_Ledger from './export/actions/Publisher_Payment_LedgerActions';
import * as actionsPublisher_Restriction from './export/actions/Publisher_RestrictionActions';
import * as actionsPublisher_deal_terms from './export/actions/Publisher_deal_termsActions';
import * as actionsPublisher_financial_info from './export/actions/Publisher_financial_infoActions';
import * as actionsRegion from './export/actions/RegionActions';
import * as actionsRestriction_Types from './export/actions/Restriction_TypesActions';
import * as actionsRevenue from './export/actions/RevenueActions';
import * as actionsRtb_Dsp_deal from './export/actions/Rtb_Dsp_dealActions';
import * as actionsRtb_deals from './export/actions/Rtb_dealsActions';
import * as actionsServiceChoices from './export/actions/ServiceChoicesActions';
import * as actionsUser from './export/actions/UserActions';
import * as actionsVerticals from './export/actions/VerticalsActions';
import * as actionsZone from './export/actions/ZoneActions';
import * as actionsZoneGrade from './export/actions/ZoneGradeActions';
import * as actionsZoneType from './export/actions/ZoneTypeActions';
import * as actionsZone_Ad_Preferences from './export/actions/Zone_Ad_PreferencesActions';
import * as actionsZone_Fixed_CPM from './export/actions/Zone_Fixed_CPMActions';
import * as actionsZone_Service from './export/actions/Zone_ServiceActions';
import * as actionsZone_Slots from './export/actions/Zone_SlotsActions';
import * as actionsZone_Tags from './export/actions/Zone_TagsActions';
import * as actionsZone_Unit_Types from './export/actions/Zone_Unit_TypesActions';
import * as actionsZone_financial_info from './export/actions/Zone_financial_infoActions';
import * as actionsZone_revenue_share from './export/actions/Zone_revenue_shareActions';
import * as actionsZones_Asset_Url_Filter_Pattern from './export/actions/Zones_Asset_Url_Filter_PatternActions';
import * as actionsZones_Class__Restriction from './export/actions/Zones_Class__RestrictionActions';
import * as actionsZones_Restriction from './export/actions/Zones_RestrictionActions';
import * as actionsdashboard_users from './export/actions/dashboard_usersActions';
import * as actionsslotsizecontroller from './export/actions/slot-size-controllerActions';

class App extends Component {

    state = {
        sigarTags: [],
        searchValue: '',
        selectedTagValue: '',
        selectedActionValue: 'NA',
        bodyParamsValue: '',
        urlParamsValues: false,
        queryParamsValues: false,
        queryParamsRequired: false,
        actionOptions: [],
        showConsole: false
    }
    componentDidMount(){
        const sigarTags = Object.keys(sigarObj).sort(function(a, b){
            if(a < b) return -1;
            if(a > b) return 1;
            return 0;
        });
        this.setState({ sigarTags });
    }

    handleChangePost = (e) => {
        this.setState({bodyParamsValue: e.target.value});
    }

    handleChangeQueryValue = e => {
        const queryParamsValues = clone(this.state.queryParamsValues);
        queryParamsValues[e.target.name] = e.target.value;
        this.setState({ queryParamsValues });
    }

    handleChangeURLValue = e => {
        const urlParamsValues = clone(this.state.urlParamsValues);
        urlParamsValues[e.target.name] = e.target.value;
        this.setState({ urlParamsValues });
    }

    handleClickAction = (selectedTag, selectedAction)=> () =>{
        const selectedTagValue  = selectedTag;
        const selectedActionValue = selectedAction;
        const item = sigarObj[selectedTagValue] ? sigarObj[selectedTagValue].filter(endpoint => endpoint.actionName === selectedActionValue)[0] : null;

        //There is nothing selected - DO NOTHING!!!!
        if (!selectedActionValue || selectedActionValue === "NA") return false;

        //Fill variable parameters
        let urlParamsValues = item && item.parameters && item.parameters.url.length > 0;
        if (urlParamsValues) {
            urlParamsValues = {};
            item.parameters.url.forEach(param => {
                urlParamsValues[param.name] = '';
            });
        }
        let queryParamsValues = false;
        let queryParamsRequired = false;
        if (item && item.parameters && item.parameters.query.length > 0){
            queryParamsValues = {};
            queryParamsRequired = {};
            item.parameters.query.forEach(param => {
                queryParamsRequired[param.name] = param.required;
                queryParamsValues[param.name] = '';
            });
        }
        const bodyParamsValue = (
            item &&
            item.parameters &&
            item.parameters.body != null &&
            (item.parameters.body.length > 0 || typeof item.parameters.body === "object")) ?
            JSON.stringify(item.parameters.body, null, 2) : '';

        this.setState({
            selectedActionValue,
            bodyParamsValue,
            urlParamsValues,
            queryParamsValues,
            queryParamsRequired,
            selectedTagValue
        });
    }
    
    _handleExecute = e => {
        const { urlParamsValues, bodyParamsValue, queryParamsValues, selectedTagValue, selectedActionValue } = this.state;
        this.props[`actions${selectedTagValue}`][selectedActionValue](urlParamsValues, queryParamsValues, bodyParamsValue);
        this.setState({showConsole: true})
    }

    updateSearch = e => {
        this.setState({
            searchValue: e.target.value
        })
    }

    buildList = () => {
        const { sigarTags, selectedActionValue, searchValue } = this.state;
        return (
            <section id="sidebar" >
                <div className="searchEndpointHolder">
                    <div className='searchEndpoint'>
                        <input type="text" value={searchValue} onChange={this.updateSearch}  />
                        <img src={lupa} alt="search" />
                    </div>
                </div>
                <div className="inner">
                        {sigarTags.map(((option, index) => {
                            const applicableActions =  searchValue ? sigarObj[option].filter(opt => opt.actionName.toLowerCase().indexOf(searchValue.toLowerCase()) > -1 ) : sigarObj[option];
                            return applicableActions.length > 0  ? (
                                <nav key={index} className="animated fadeInUp">
                                <div className="menu-title">Tag {option}</div>
                                <ul>
                                    {
                                        applicableActions.map(item => item.actionName).map(((act, i) => (
                                            <li key={i} >
                                                <a 
                                                    onClick={this.handleClickAction(option, act)}
                                                    className={selectedActionValue === act ? 'active' : ''}
                                                >
                                                {act}
                                            </a>
                                        </li>
                                    )))
                                    }
                                </ul>
                                </nav>
                            ): null;
                        }
                        ))}
                </div>
			</section>
        )
    }

    toggleConsole = () => {
        this.setState(({ showConsole }) => ({ showConsole: !showConsole }));
    }

    buildParamTables = () => {
        const { selectedActionValue, bodyParamsValue, urlParamsValues, queryParamsValues, queryParamsRequired } = this.state;
        if (!selectedActionValue || selectedActionValue === "NA") return ''; //There is nothing selected - DO NOTHING!!!!
        return  (
            (urlParamsValues && Object.keys(urlParamsValues).length > 0) ||
            (queryParamsValues && Object.keys(queryParamsValues).length > 0) ||
             bodyParamsValue
            ) ? (
            <div>
                <h1 className="title">{selectedActionValue}</h1>
                {urlParamsValues && (
                    <div>
                        <h2>URL Parameters</h2>
                        <table>
                            <thead>
                                <tr>
                                    <th>Parameter</th>
                                    <th>required</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                {Object.keys(urlParamsValues).map((key,index) => {
                                    return(
                                        <tr key={index}>
                                            <td> {key}</td>
                                            <td><span className="labelGreen">yes</span></td>
                                            <td>
                                                <input
                                                    type="text"
                                                    name={key}
                                                    value={urlParamsValues[key]}
                                                    onChange={this.handleChangeURLValue}
                                                />
                                            </td>
                                        </tr>
                                    )
                                })}

                            </tbody>
                        </table>
                    </div>
                )}
                {queryParamsValues && (
                    <div>
                        <h2>Query Parameters</h2>
                        <table>
                            <thead>
                                <tr>
                                    <th>Parameter</th>
                                    <th>required</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                {Object.keys(queryParamsValues).map((key,index) => {
                                    const required = queryParamsRequired[key] ? (<span className="labelGreen">yes</span>) : (<span className="labelRed">no</span>);
                                    return(
                                        <tr key={index}>
                                            <td>{key}</td>
                                            <td>{required}</td>
                                            <td>
                                                <input
                                                    name={key}
                                                    value={queryParamsValues[key]}
                                                    type="text"
                                                    onChange={this.handleChangeQueryValue}
                                                />
                                            </td>
                                        </tr>
                                    )
                                })}

                            </tbody>
                        </table>
                    </div>
                )}
                {bodyParamsValue && (
                    <div>
                        <h2>Body Parameters</h2>
                        <div className="full-width-forms">
                            <textarea
                                style={{minHeight: '300px', border: '1px solid black'}}
                                value={bodyParamsValue}
                                onChange={this.handleChangePost}
                            />
                        </div>
                    </div>
                )}
                <div className="btn small execute params" onClick={this._handleExecute} >
                    Try
                </div>
            </div>
        ): '';
    }
    getConsoleMsgs = () => {
        const log = this.props.data.console ? this.props.data.console : []
        let msgString = '';
        for (let i=0;i<log.length;i++){
            msgString += log[i]
        }
        return msgString;
    }

    render() {

        const { selectedActionValue, showConsole } = this.state;
        const loggerClass = showConsole ? 'open' : 'close'
        const hamburgerClass = showConsole ? 'hamburger active' : 'hamburger'
        const showAll = selectedActionValue === "NA" ? "no-active": "animated fadeInUp";
        const messages = this.getConsoleMsgs();

        return (
            <div>
            {this.buildList()}
                <div id="wrapper">
                    <section id="intro" className="wrapper style1 fullscreen fade-up">
                        <div id="logoHolder">
                            <img src={logo} className="logo" alt="sigar" />
                            <div className="logo-text">SIGAR</div>
                        </div>
                        <div className={"row section fede " + showAll}>
                            {this.buildParamTables()}
                        </div>
                    </section>
                    <div className={"row " + showAll} id="consoleHolder">
                        <div id="action-bar">
                            <span>Console</span>
                            <div className="buttons">
                            
                                <div className="btn small execute" onClick={this._handleExecute} >
                                    Try
                                </div>
                                <div className={hamburgerClass} id="ham-2" onClick={this.toggleConsole}>
                                    <div className="line"></div>
                                    <div className="line"></div>
                                    <div className="line"></div>
                                </div>
                            </div>
                        </div>
                        <div id="logger" className={loggerClass}>
                            <textarea id="consoleLogger" value={messages} onChange={() => {}} />
                        </div>
                    </div>
                        
                
            </div>
        </div>
	);
  }
}
function mapDispatchToProps(dispatch) {
    return {
         actionsActivityLog: bindActionCreators(actionsActivityLog, dispatch),
         actionsAdProduct: bindActionCreators(actionsAdProduct, dispatch),
         actionsAdUnitTypeGroup: bindActionCreators(actionsAdUnitTypeGroup, dispatch),
         actionsAdUnitTypes: bindActionCreators(actionsAdUnitTypes, dispatch),
         actionsAd_Buys: bindActionCreators(actionsAd_Buys, dispatch),
         actionsAdvertisers: bindActionCreators(actionsAdvertisers, dispatch),
         actionsBroswer_Family: bindActionCreators(actionsBroswer_Family, dispatch),
         actionsBroswer_Versions: bindActionCreators(actionsBroswer_Versions, dispatch),
         actionsBrowser_Type: bindActionCreators(actionsBrowser_Type, dispatch),
         actionsBrowser_Type_Group: bindActionCreators(actionsBrowser_Type_Group, dispatch),
         actionsBusiness_Unit: bindActionCreators(actionsBusiness_Unit, dispatch),
         actionsCampaigns: bindActionCreators(actionsCampaigns, dispatch),
         actionsCity: bindActionCreators(actionsCity, dispatch),
         actionsCountry: bindActionCreators(actionsCountry, dispatch),
         actionsEmbedCodes: bindActionCreators(actionsEmbedCodes, dispatch),
         actionsExport_Ads_txt_Reseller_Entries: bindActionCreators(actionsExport_Ads_txt_Reseller_Entries, dispatch),
         actionsImplementation_type: bindActionCreators(actionsImplementation_type, dispatch),
         actionsIndustry: bindActionCreators(actionsIndustry, dispatch),
         actionsIndustryCategory: bindActionCreators(actionsIndustryCategory, dispatch),
         actionsInfo: bindActionCreators(actionsInfo, dispatch),
         actionsMetro_Code: bindActionCreators(actionsMetro_Code, dispatch),
         actionsOsFamilyName: bindActionCreators(actionsOsFamilyName, dispatch),
         actionsPixel_Black_List: bindActionCreators(actionsPixel_Black_List, dispatch),
         actionsPublisher: bindActionCreators(actionsPublisher, dispatch),
         actionsPublisher_Payment_Info: bindActionCreators(actionsPublisher_Payment_Info, dispatch),
         actionsPublisher_Payment_Ledger: bindActionCreators(actionsPublisher_Payment_Ledger, dispatch),
         actionsPublisher_Restriction: bindActionCreators(actionsPublisher_Restriction, dispatch),
         actionsPublisher_deal_terms: bindActionCreators(actionsPublisher_deal_terms, dispatch),
         actionsPublisher_financial_info: bindActionCreators(actionsPublisher_financial_info, dispatch),
         actionsRegion: bindActionCreators(actionsRegion, dispatch),
         actionsRestriction_Types: bindActionCreators(actionsRestriction_Types, dispatch),
         actionsRevenue: bindActionCreators(actionsRevenue, dispatch),
         actionsRtb_Dsp_deal: bindActionCreators(actionsRtb_Dsp_deal, dispatch),
         actionsRtb_deals: bindActionCreators(actionsRtb_deals, dispatch),
         actionsServiceChoices: bindActionCreators(actionsServiceChoices, dispatch),
         actionsUser: bindActionCreators(actionsUser, dispatch),
         actionsVerticals: bindActionCreators(actionsVerticals, dispatch),
         actionsZone: bindActionCreators(actionsZone, dispatch),
         actionsZoneGrade: bindActionCreators(actionsZoneGrade, dispatch),
         actionsZoneType: bindActionCreators(actionsZoneType, dispatch),
         actionsZone_Ad_Preferences: bindActionCreators(actionsZone_Ad_Preferences, dispatch),
         actionsZone_Fixed_CPM: bindActionCreators(actionsZone_Fixed_CPM, dispatch),
         actionsZone_Service: bindActionCreators(actionsZone_Service, dispatch),
         actionsZone_Slots: bindActionCreators(actionsZone_Slots, dispatch),
         actionsZone_Tags: bindActionCreators(actionsZone_Tags, dispatch),
         actionsZone_Unit_Types: bindActionCreators(actionsZone_Unit_Types, dispatch),
         actionsZone_financial_info: bindActionCreators(actionsZone_financial_info, dispatch),
         actionsZone_revenue_share: bindActionCreators(actionsZone_revenue_share, dispatch),
         actionsZones_Asset_Url_Filter_Pattern: bindActionCreators(actionsZones_Asset_Url_Filter_Pattern, dispatch),
         actionsZones_Class__Restriction: bindActionCreators(actionsZones_Class__Restriction, dispatch),
         actionsZones_Restriction: bindActionCreators(actionsZones_Restriction, dispatch),
         actionsdashboard_users: bindActionCreators(actionsdashboard_users, dispatch),
         actionsslotsizecontroller: bindActionCreators(actionsslotsizecontroller, dispatch),
    };
}


function mapStateToProps(state) {
    return {
        data: state,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

