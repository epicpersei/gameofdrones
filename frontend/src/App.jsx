//vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


//styles
import './App.css';

//actions
import * as appActions from './actions/appActions';

//components
import Welcome from './components/welcome';
import Scoreboard from './components/scoreboard';
import Game from './components/game';
import MovesSettings from './components/move-settings';


class App extends Component {

    state = {
        welcome: true,
        viewScores: false,
        gameRunning: false,
        showMovesSettings: false,
    }

    componentDidMount() {
        this.props.appActions.getScores();
        this.props.appActions.getMoves();
    }

    toggleViewScores = () => {
        this.setState(({ viewScores }) => ({ viewScores: !viewScores }));
    }

    toggleWelcome = () => {
        this.setState(({ welcome }) => ({ welcome: !welcome }));
    }

    toggleMovesSettings = () => {
        this.setState(({ showMovesSettings }) => ({ showMovesSettings: !showMovesSettings }));
    }

    endGamePlay = ( result ) => () => {
        const { appActions } = this.props;
        appActions.postScores(result)
        this.setState({
            gameRunning: false,
            welcome: true
        }, ()=> { appActions.getScores() })
    }

    toggleGame = () => {
        this.setState(({ gameRunning }) => ({ 
            gameRunning: !gameRunning,
            viewScores: false,
            showMovesSettings: false,
        }));
    }
    
    render() {
        const { scores, moves, appActions } = this.props;
        const { viewScores, welcome, gameRunning, showMovesSettings } = this.state;
        return (
            <div className="App container">
                <h1>Game of Drones</h1>
                {welcome && (
                    <Welcome
                        className="animated fadeInDown"
                        viewScores={() => {
                            this.toggleWelcome();
                            this.toggleViewScores();
                        }}
                        playGame={() => {
                            this.toggleWelcome();
                            this.toggleGame();
                        }}
                        showMovesSettings={()=> {
                            this.toggleWelcome();
                            this.toggleMovesSettings()
                        }}
                    />
                )}
                {viewScores && (
                    <Scoreboard
                        className="animated flipInX"
                        scores={scores}
                        style={{textAlign: 'left'}}
                        playGame={this.toggleGame}
                    />
                )}

                {gameRunning && (
                    <Game endGamePlay ={this.endGamePlay} className="animated fadeIn"/>
                )}
                {showMovesSettings && (
                    <MovesSettings
                        moves={moves}
                        postMove={appActions.postMove}
                        deleteMove={appActions.deleteMove}
                        playGame={this.toggleGame}
                    />
                )}
                
            </div>
        );
    }
}

App.propTypes = {
    appActions: PropTypes.object,
    scores: PropTypes.array,
    moves: PropTypes.array
};

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch),
    };
}


function mapStateToProps(state) {
    return {
        scores: state.app.scores,
        moves: state.app.moves
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

