import * as types from '../constants/actionTypes';
import { CALL_API } from '../assets/api/api';
import { URIBuilder } from '../assets/URIBuilder';

export const postScores = (data) => ({
    [CALL_API]: {
        types: [
            types.POST_SCORES_REQUEST,
            types.POST_SCORES_SUCCESS,
            types.POST_SCORES_FAILURE
        ],
        endpoint: URIBuilder('scores'),
        options: {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
    }
});

export const getScores = () => ({
    [CALL_API]: {
        types: [
            types.GET_SCORES_REQUEST,
            types.GET_SCORES_SUCCESS,
            types.GET_SCORES_FAILURE
        ],
        endpoint: URIBuilder('scores'),
        options: {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }
});


export const getMoves = (queryObject) => ({
    [CALL_API]: {
        types: [
            types.GET_MOVES_REQUEST,
            types.GET_MOVES_SUCCESS,
            types.GET_MOVES_FAILURE
        ],
        endpoint: URIBuilder('moves', queryObject),
        options: {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }
    }
});

export const postMove = (data) => ({
    [CALL_API]: {
        types: [
            types.POST_MOVE_REQUEST,
            types.POST_MOVE_SUCCESS,
            types.POST_MOVE_FAILURE
        ],
        endpoint: URIBuilder('moves'),
        options: {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
    }
});

export const deleteMove = (id) => ({
    [CALL_API]: {
        moveId: id,
        types: [
            types.DELETE_MOVE_REQUEST,
            types.DELETE_MOVE_SUCCESS,
            types.DELETE_MOVE_FAILURE
        ],
        endpoint: URIBuilder('deleteMoves', {id}),
        options: {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }
});
