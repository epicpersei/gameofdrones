import { toPairs, map, join, compose, concat } from 'ramda';
import { endpoints } from '../constants/endpoints';
const createQueryString = compose(concat('?'), join('&'), map(join('=')), toPairs);
const API_URL = 'http://localhost:8080'

export const URIBuilder = (endpoint, params = {}, queryStringObject) => {
    let uri = API_URL + endpoints[endpoint];
    if (params) {
        const keys = Object.keys(params);
        keys.forEach(key => {
            const values = params[key];
            if (Array.isArray(values)) {
                const filtersString = values.reduce((acc, val) => acc.concat(val + ','), '');
                uri = uri.replace('[' + key + ']', filtersString.slice(0, -1));
            } else {
                uri = uri.replace('[' + key + ']', values);
            }
        });
    }
    const queryString = queryStringObject && createQueryString(queryStringObject);
    uri = queryString ? uri + queryString : uri;
    return uri;
};
export default URIBuilder;
