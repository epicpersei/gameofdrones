import appendHeaders from './appendHeaders';
import parseResponse from './parseResponse';
import errorHandler from './errorHandler';

// Fetches an API response
export const callApi = (endpoint, newOptions) => {
    const { headers = {} } = newOptions || {};

    // Default fetch options
    let fetchOptions =  {
        ...newOptions,
        headers: new Headers({
            ...headers,
        })
    };

    return fetch(endpoint, fetchOptions)
        .then(errorHandler)
        .then(parseResponse)
        .then(data => appendHeaders(data, endpoint));
};

// Action key that carries API call info interpreted by this Redux middleware.
export const CALL_API = 'Call API';

// A Redux middleware that interprets actions with CALL_API info specified.
// Performs the call and promises when such actions are dispatched.
export default store => next => action => {
    const callAPI = action[CALL_API];
    const currentState = store.getState();

    if (typeof callAPI === 'undefined') {
        return next(action);
    }

    let { endpoint } = callAPI;
    const { types, options, ...extraPayload } = callAPI;

    if (typeof endpoint === 'function') {
        endpoint = endpoint(currentState);
    }

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.');
    }

    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.');
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('Expected action types to be strings.');
    }

    const actionWith = data => {
        const finalAction = Object.assign({}, action, data);
        delete finalAction[CALL_API];
        return finalAction;
    };

    const { callback } = callAPI;

    const [requestType, successType, failureType] = types;
    next(actionWith({ type: requestType, ...extraPayload }));

    return callApi(endpoint, options).then(
        response => {
            if (callback) callback();
            next(
                actionWith({
                    response,
                    ...extraPayload,
                    type: successType
                })
            );
        },
        error =>
            error.json().then(
                body =>
                    body.code === 401 // Unauthorized response
                        ? next(actionWith({ type: 'AUTH_ERROR' }))
                        : next(
                              actionWith({
                                  ...extraPayload,
                                  type: failureType,
                                  errors: body,
                                  status: error.status
                              })
                          )
            )
    );
};
