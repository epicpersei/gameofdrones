/**
 * Appends the token if we are using the 'login' endpoint, and removes the user from the wrapper array
 * @param   {Array}    data
 * @param   {String}   endpoint
 * @returns {Object}
 */
const appendHeaders = (data, endpoint) => {
    const [response] = data;

    return endpoint.includes('auth')
        ? Object.assign({}, response.data.user, { jwt: response.data.token })
        : response;
};

export default appendHeaders;
