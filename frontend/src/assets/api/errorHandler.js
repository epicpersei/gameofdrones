/**
 * Fetch errors handler
 * @param   {Object}    response
 * @returns {Object}
 */
const errorHandler = response => (response.ok ? response : Promise.reject(response));

export default errorHandler;
