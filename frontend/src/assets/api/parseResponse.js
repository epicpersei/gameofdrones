/**
 * Extracts the authorization on the login endpoint
 * @param   {Object}    response
 * @returns {Promise}
 */
const parseResponse = response => {
    const emptyResponse = response.status === 204,
        emptyPromise = new Promise(r => r([{}]));
    return Promise.all([
        emptyResponse ? emptyPromise : response.json(),
        response.headers.get('Authorization')
    ]);
};

export default parseResponse;
