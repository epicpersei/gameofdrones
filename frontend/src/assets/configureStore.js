/**
 * NOTE
 * This store configuration is not optimized for production, it is basically
 * only for development, and includes hot-reload in mind
 */
import { createStore, compose, applyMiddleware } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunkMiddleware from 'redux-thunk';
import api from './api/api.js';
import reducers from '../reducers';

//import { routerMiddleware } from 'react-router-redux';

//const routerWare = routerMiddleware(history);

const configureStore = initialState => {
    const middlewares = [
        // Add more middleware here
        reduxImmutableStateInvariant(),
        thunkMiddleware,
  //      routerWare,
        api
    ];

    const store = createStore(
        reducers,
        initialState,
        compose(
            applyMiddleware(...middlewares),
            //Redux devToolsExtension
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    );

    if (module.hot) {
        // Hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers').default; // eslint-disable-line global-require
            store.replaceReducer(nextReducer);
        });
    }

    return store;
};

export default configureStore;
