const initialState = []

export default function consoleReducer(state = initialState, action) {
    // Catch all
    if (action.type) {
        const logMsg = `${action.type}
        ${JSON.stringify(action, null, 2)}
        
`;
        return [logMsg, ...state];
    } 
    else {
        return state
    }
}
