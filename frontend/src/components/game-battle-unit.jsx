import React from 'react';
import PropTypes from 'prop-types';
import piedra from '../images/landslide.png' ;
import papel from '../images/legal-paper.png' ;
import tijera from '../images/cut.png' ;

const GameBattleUnit = ({ playerName, doSelection, moveOptions, playerReady }) => (
    <div>
        <h2>{playerName}</h2>
        {playerReady ? (
            <div className="alert alert-success" role="alert">
                ready
            </div>
        ): (
            <div className="alert alert-danger" role="alert">
                not ready
            </div>
        )}
        {
            moveOptions.map((option, index) => {
                switch (option) {
                    case 'rock':
                        return <img key={index} className="weapon" alt="weapon" src={piedra} onClick={doSelection(option)} />
                    case 'paper':
                        return <img key={index} className="weapon" alt="weapon" src={papel} onClick={doSelection(option)} />
                    case 'scissors':
                        return <img key={index} className="weapon" alt="weapon" src={tijera} onClick={doSelection(option)} />
                    default:
                        return <button key={index} className="weapon" onClick={doSelection(option)} >{option}</button>
                }
            })
        }
    </div>
)

GameBattleUnit.defaultProps = {
    playerName: 'No Name set',
    playerReady: false,
    moveOptions: ['rock', 'paper', 'scissors'],
};

GameBattleUnit.propTypes = {
    playerName: PropTypes.string,
    moveOptions: PropTypes.array,
    doSelection: PropTypes.func,
    playerReady: PropTypes.bool
};

export default GameBattleUnit;
