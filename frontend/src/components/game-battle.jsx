//vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import GameBattleUnit from './game-battle-unit';

class GameBattle extends Component {
    state = {
        player1Selected: null,
        player2Selected: null,
        showError: false
    }

    doSelection = player => option => () => {
        this.props.setParentStateField(player, option);
    }

    render() {
        const { names, doTry, selections: { player1Selected, player2Selected }, moveOptions } = this.props
        let title = 'Choose your weapon'; 
        let player1Ready = (!!player1Selected)
        let player2Ready = (!!player2Selected)
        title = player1Selected === null && player2Selected ? `${names.player1} please choose your weapon` : title;
        title = player2Selected === null && player1Selected ? `${names.player2} please choose your weapon` : title;
        title = player1Selected && player2Selected ? `Hit try!` : title;
        return (
            <div>
                <h3>{title}</h3>
                <div className="row">
                    <div className="col">
                        <GameBattleUnit
                            playerName={names.player1}
                            playerReady={player1Ready}
                            doSelection={this.doSelection('player1Selected')}
                            moveOptions={moveOptions}
                        />
                    </div> 
                    <div className="col">
                        <GameBattleUnit
                            playerName={names.player2}
                            playerReady={player2Ready}
                            doSelection={this.doSelection('player2Selected')}
                            moveOptions={moveOptions}
                        />
                    </div> 
                </div>
                <div className="row">    
                    <button
                        className="btn btn-primary"
                        style={{top: '30px',margin: '0 auto'}}
                        onClick={doTry}
                    >
                        Try
                    </button>
                </div>
            </div>
        );
    }
}

GameBattle.defaultProps = {
    names: {
        player1: 'Player 1',
        player2: 'Player 2'
    }
}

GameBattle.propTypes = {
    doTry: PropTypes.func,
    names: PropTypes.object,
    selections: PropTypes.object,
    setParentStateField: PropTypes.func,
    moveOptions: PropTypes.array
};


export default GameBattle;

