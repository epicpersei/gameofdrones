//vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
class GameNames extends Component {

    state = {
        player1: '',
        player2: '',
        player1InValid: false,
        player2InValid: false,
        showNoWeaponSelected: false,
    }

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    validateName = (name) => (name.trim() === '')

    nextStep = () => {
        const { player1, player2 } = this.state;
        const player1InValid = this.validateName(player1);
        const player2InValid = this.validateName(player2);

        this.setState({ player1InValid, player2InValid}, ()=>{
            if (!player1InValid && !player2InValid) {
                this.props.continue({ player1, player2})
            }
        })
    }
    
    render() {
        const { player1, player2, player1InValid, player2InValid } = this.state;
        const renderPlayer1 = player1.trim() ? player1 : 'Player 1';
        const renderPlayer2 = player2.trim() ? player2 : 'Player 2';
        const player1Class = cn('form-control',{ 'is-invalid': player1InValid });
        const player2Class = cn('form-control',{ 'is-invalid': player2InValid });
        return (
            <div>
                <h3>Please enter your names</h3>
                <div className="row">
                    <div className="col">
                        <h2>{renderPlayer1}</h2>
                        <div className="form-group">
                            <input
                                type="text"
                                className={player1Class}
                                name="player1"
                                placeholder="Enter your name player 1"
                                value={player1}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div> 
                    <div className="col">
                        <h2>{renderPlayer2}</h2>
                        <div className="form-group">
                            <input
                                type="text"
                                className={player2Class}
                                name="player2"
                                placeholder="Enter your name player 2"
                                value={player2}
                                onChange={this.handleChange}
                            />
                        </div>
                    </div> 
                </div>
                <div className="row">    
                    <button
                        className="btn btn-primary"
                        id="continue"
                        style={{top: '30px',margin: '0 auto'}}
                        onClick={this.nextStep}
                    >
                        Continue
                    </button>
                </div>
            </div>
        );
    }
}

GameNames.defaultProps = {
    continue: () => {
    }
}

GameNames.propTypes = {
    continue: PropTypes.func
};


export default GameNames;

