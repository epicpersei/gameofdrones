//vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//actions
import * as appActions from '../actions/appActions';

//components
import GameNames from './game-names';
import GameBattle from './game-battle';

class Game extends Component {

    state = {
        names: false,
        tries: 0,
        player1Wins: 0,
        player2Wins: 0,
        player1Selected: null,
        player2Selected: null,
        showScore: false,
        showWeaponError: false,
        showEven: false,
    }

    componentDidMount() {
        this.props.appActions.getMoves();
    }

    setNames = (names) => {
        this.setState({names})
    }

    setParentStateField = (stateField, val) => {
        this.setState({
            [stateField]: val
        });
    }

    endGamePlay = () => {
        this.setState({
            player1Selected: null,
            player2Selected: null,
            showWeaponError: false,
            showWinner: false
        },this.props.endGamePlay({ ...this.state}));
    }

    resetBattle = () => {
        if (!this.state.showWinner) {
            this.setState({
                player1Selected: null,
                player2Selected: null,
                showWeaponError: false,
            });
        }
    }

    doTry = () =>{
        const { player1Wins, player2Wins, player1Selected, player2Selected, tries } = this.state
        if (player1Selected && player2Selected) {
            const { moves } = this.props;
            const player1Move = moves.filter(opt => opt.move === player1Selected)[0]
            const player2Move = moves.filter(opt => opt.move === player2Selected)[0]
            if (player1Move.kills === player2Move.move) {
                const weHaveAWinner = (player1Wins + 1) >= 3
                this.setState({ player1Wins: player1Wins + 1, tries: tries + 1, showScore: !weHaveAWinner, showWinner: weHaveAWinner }, this.resetBattle);
            } else if (player2Move.kills === player1Move.move) {
                const weHaveAWinner = (player2Wins + 1) >= 3
                this.setState({ player2Wins: player2Wins + 1, tries: tries + 1, showScore: !weHaveAWinner, showWinner: weHaveAWinner }, this.resetBattle);
            } else {
                this.setState({showEven: true, tries: tries + 1 }, this.resetBattle);
            }

        } else {
            this.setState({showWeaponError: true})
        }
    }

    toggleWeaponError = () => {
        this.setState(({ showWeaponError }) => ({ showWeaponError: !showWeaponError }));
    }

    buildWeaponError = () => {
        return (
            <div className="modal fade show" role="dialog" style={{display: 'block', background: 'rgba(0,0,0,0.5)'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <p>Both players must select a weapon.</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.toggleWeaponError} >Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    buildWinner = () => {
        const { player1Wins, player2Wins, names: {player1, player2 } } = this.state;
        const winner = (player1Wins > player2Wins) ? player1 : player2;
        return (player1Wins >= 3 || player2Wins >= 3) ? (
            <div className="modal fade show" role="dialog" style={{display: 'block', background: 'rgba(0,0,0,0.5)'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">We have a Winner!</h5>
                        <button type="button" className="close" onClick={this.toggleWinner} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div className="modal-body">
                            <p>{winner} is the winner!</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.endGamePlay} >Close</button>
                        </div>
                    </div>
                </div>
            </div>
        ) : null
    }

    toggleEven = () => {
        this.setState(({ showEven }) => ({ showEven: !showEven }));
    }

    buildEven = () => {
        return (
            <div className="modal fade show" role="dialog" style={{display: 'block', background: 'rgba(0,0,0,0.5)'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">You both choose the same weapon!</h5>
                        <button type="button" className="close" onClick={this.toggleEven} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div className="modal-body">
                            <p>This round goes even! </p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.toggleEven} >Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    toggleScore = () => {
        this.setState(({ showScore }) => ({ showScore: !showScore }));
    }

    buildScore = () => {
        const { player1Wins, player2Wins, names } = this.state
        const winner = (player1Wins > player2Wins) ? 1 : 2;
        return (
            <div className="modal fade show" role="dialog" style={{display: 'block', background: 'rgba(0,0,0,0.5)'}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Player {winner} wins this Round!</h5>
                        <button type="button" className="close" onClick={this.toggleScore}  aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div className="modal-body">
                            <p>{names.player1} {player1Wins} VS {player2Wins} {names.player2} </p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" onClick={this.toggleScore} >Close</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const { className, moves } = this.props;
        const { names, showScore, player1Selected , player2Selected, showWeaponError, showEven, showWinner } = this.state;
        const moveOptions = moves.map(move => move.move);
        return (
            <div className={className}>
                {showScore && this.buildScore()}
                {showWeaponError && this.buildWeaponError()}
                {showEven && this.buildEven()}
                {showWinner && this.buildWinner()}
                {names === false && (<GameNames continue={this.setNames} />)}
                {names && (
                    <GameBattle
                        names={names}
                        doTry={this.doTry}
                        setParentStateField={this.setParentStateField}
                        selections={{ player1Selected, player2Selected }}
                        moveOptions={moveOptions}
                    />
                )}
            </div>
        );
    }
}
function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch),
    };
}


function mapStateToProps(state) {
    return {
        moves: state.app.moves,
    };
}
Game.defaultProps = {
    className: ""
}

Game.propTypes = {
    className: PropTypes.string,
    appActions: PropTypes.object,
    endGamePlay: PropTypes.func,
    moves: PropTypes.array,
};


export default connect(mapStateToProps, mapDispatchToProps)(Game);

