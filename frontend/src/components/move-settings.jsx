//vendors
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { clone, remove } from 'ramda';

const emptyMove = {
    move: '',
    kills: ''
}

class MovesSettings extends Component {
    state = {
        movesToAdd: []
    }
    addNewRow = () => {
        const movesToAdd = clone(this.state.movesToAdd);
        movesToAdd.push(emptyMove);
        this.setState({ movesToAdd });
    }

    handleChangeInput = (e) => {
        const type = e.target.name.split('-')[0];
        const index = e.target.name.split('-')[1];
        const movesToAdd = clone(this.state.movesToAdd);
        movesToAdd[index][type] = e.target.value;
        this.setState({
            movesToAdd
        })
    }

    handleSaveRow = index => () => {
        const moveToAdd = this.state.movesToAdd[index];
        this.props.postMove(moveToAdd);
        const movesToAdd = remove(index, 1, this.state.movesToAdd);
        this.setState({ movesToAdd });
    }

    handleDeleteRow = moveId => () => {
        this.props.deleteMove(moveId);
    }

    render() {
        const { moves, playGame } = this.props;
        const { movesToAdd } = this.state;
        return (
            <div>
                <h3>Moves Settings</h3>
                <table className="table" style={{textAlign: 'left'}}>
                    <thead>
                        <tr>
                            <th>Move</th>
                            <th>Kills</th>
                            <th />
                            
                        </tr>
                    </thead>
                    <tbody>
                        {moves.map((move, index) =>(
                            <tr key={index}>
                                <td>{move.move}</td>
                                <td>{move.kills}</td>
                                <td>
                                    <button className="btn btn-danger btn-sm" onClick={this.handleDeleteRow(move.id)}>
                                        <i className="far fa-trash-alt" />
                                    </button>
                                </td>
                            </tr>
                        ))}
                        {movesToAdd.map((move, index) =>(
                            <tr key={index}>
                                <td>
                                    <input
                                        style={{width: '200px'}}
                                        className="form-control"
                                        value={move.move}
                                        type="text"
                                        name={`move-${index}`}
                                        onChange={this.handleChangeInput}
                                    />
                                </td>
                                <td>
                                    <input
                                        style={{width: '200px'}}
                                        className="form-control"
                                        value={move.kills}
                                        type="text"
                                        name={`kills-${index}`}
                                        onChange={this.handleChangeInput}
                                    />
                                </td>
                                <td>
                                    <button className="btn btn-success btn-sm" onClick={this.handleSaveRow(index)}>
                                        <i className="far fa-save" />
                                    </button>
                                </td>
                            </tr>
                        ))}
                         <tr>
                                <td />
                                <td />
                                <td>
                                    <button className="btn btn-success btn-sm" onClick={this.addNewRow}>
                                        <i className="fas fa-plus" />
                                    </button>
                                </td>
                            </tr>
                    </tbody>
                </table>
                <div className="text-center">
                    <button className="btn btn-dark deleteMove" id="playGameBtn" onClick={playGame}>Play</button>
                </div>
            </div>
        );
    }
}

MovesSettings.defaultProps = {
    moves: []
}

MovesSettings.propTypes = {
    moves: PropTypes.array,
    postMove: PropTypes.func,
    deleteMove: PropTypes.func,
    playGame: PropTypes.func
};


export default MovesSettings;

