import React from 'react';
import PropTypes from 'prop-types';

const Scoreboard = ({ playGame, scores, ...otherProps }) => (
    <div {...otherProps}>
        <h3>Scoreboard</h3>
        <table className="table">
            <thead>
                <tr>
                    <th>Player</th>
                    <th># Matches</th>
                    <th># Matches Won</th>
                    <th># Matches Loss</th>
                    <th># Tries</th>
                    <th># Tries Won</th>
                </tr>
            </thead>
            <tbody>
                {scores.map((score, index) =>(
                    <tr key={index}>
                        <td>{score.player_name}</td>
                        <td>{score.matches}</td>
                        <td>{score.wins}</td>
                        <td>{score.lost}</td>
                        <td>{score.tries}</td>
                        <td>{score.tries_win}</td>
                    </tr>
                ))}
            </tbody>
        </table>
        <div className="text-center">
            <button className="btn btn-dark " onClick={playGame}>Play</button>
        </div>
    </div>
)


Scoreboard.defaultProps = {
    playGame: () => {},
    scores: []
};

Scoreboard.propTypes = {
    playGame: PropTypes.func,
    scores: PropTypes.array
};

export default Scoreboard;
