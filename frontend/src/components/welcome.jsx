import React from 'react';
import PropTypes from 'prop-types';

const Welcome = ({ playGame, viewScores, showMovesSettings, ...otherProps }) => (
    <div {...otherProps} style={{marginTop: '30px'}}>
        <button className="btn btn-success " onClick={playGame}>Play</button>
        <button className="btn btn-primary marginLeft20" onClick={viewScores}>Check Scoreboard</button>
        <button className="btn btn-danger marginLeft20" onClick={showMovesSettings}>Moves Settings</button>
    </div>
)


Welcome.defaultProps = {
    playGame: () => {},
    viewScores: () => {},
    showMovesSettings: () => {},
};

Welcome.propTypes = {
    playGame: PropTypes.func,
    viewScores: PropTypes.func,
    showMovesSettings: PropTypes.func,
};

export default Welcome;
