export const endpoints = {
    scores: '/scores',
    moves: '/moves',
    deleteMoves: '/moves/[id]',
};
