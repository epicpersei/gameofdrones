import * as types from '../constants/actionTypes';  
import { assoc, clone } from 'ramda';

const initialState = {
    moves: [],
    scores: []
}

export default function appReducer(state = initialState, action) {
    const data = action.response;

    switch (action.type) {

        case types.GET_SCORES_SUCCESS:
            return assoc('scores', data, state);

        case types.GET_MOVES_SUCCESS:
            return assoc('moves', data, state);

        case types.POST_MOVE_SUCCESS: {
            const newMoves = clone(state.moves);
            newMoves.push(data);
            return assoc('moves', newMoves, state);
        }

        case types.DELETE_MOVE_SUCCESS: {
            const { moveId } = data;
            const newState = state.moves.filter(move => move.id !== moveId);
            return assoc('moves', newState, state);
        }

        default:
             return state;
        }
  }

