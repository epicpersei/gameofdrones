import fetchMock from 'fetch-mock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../actions/appActions';
import api from '../assets/api/api';
import URIBuilder from '../assets/URIBuilder';

const middlewares = [thunk, api];
const mockStore = configureMockStore(middlewares);

describe('appActions', () => {
    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore();
    });

    it('getScores action', () => {
        const response = [];
        const endpoint = URIBuilder('scores');
        fetchMock.getOnce(endpoint, {
            body: response,
            headers: {
                'content-type': 'application/json'
            }
        });
        const expectedActions = [
            { endpoint, type: 'GET_SCORES_REQUEST' },
            { endpoint, response, type: 'GET_SCORES_SUCCESS' }
        ];
        const store = mockStore();
        store.dispatch(actions.getScores()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('postScore action', () => {
        const response = [];
        const endpoint = URIBuilder('scores');
        fetchMock.postOnce(endpoint, {
            body: response,
            headers: {
                'content-type': 'application/json'
            }
        });
        const expectedActions = [
            { endpoint, type: 'POST_SCORES_REQUEST' },
            { endpoint, response, type: 'POST_SCORES_SUCCESS' }
        ];
        const store = mockStore();
        store.dispatch(actions.postScores()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('getMoves action', () => {
        const response = [];
        const endpoint = URIBuilder('moves');
        fetchMock.getOnce(endpoint, {
            body: response,
            headers: {
                'content-type': 'application/json'
            }
        });
        const expectedActions = [
            { endpoint, type: 'GET_MOVES_REQUEST' },
            { endpoint, response, type: 'GET_MOVES_SUCCESS' }
        ];
        const store = mockStore();
        store.dispatch(actions.getMoves()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('postMove action', () => {
        const response = [];
        const endpoint = URIBuilder('moves');
        fetchMock.postOnce(endpoint, {
            body: response,
            headers: {
                'content-type': 'application/json'
            }
        });
        const expectedActions = [
            { endpoint, type: 'POST_MOVE_REQUEST' },
            { endpoint, response, type: 'POST_MOVE_SUCCESS' }
        ];
        const store = mockStore();
        store.dispatch(actions.postMove()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('deleteMove action', () => {
        const response = [];
        const endpoint = URIBuilder('deleteMoves', { id: 3 });
        fetchMock.deleteOnce(endpoint, {
            body: response,
            headers: {
                'content-type': 'application/json'
            }
        });
        const expectedActions = [
            { endpoint, moveId: 3, type: 'DELETE_MOVE_REQUEST' },
            { endpoint, moveId: 3, response, type: 'DELETE_MOVE_SUCCESS' }
        ];
        const store = mockStore();
        store.dispatch(actions.deleteMove(3)).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});
