import reducer from '../reducers/appReducer';
import * as types from '../constants/actionTypes';

const initialState = {
    moves: [],
    scores: []
}

const scores = [
    {
        player_name: "test",
        matches: 3,
        wins: 3,
        lost: 0,
        tries: 9,
        tries_win: 9
    }
];

const moves = [
    {
        id: 1,
        move: "paper",
        kills: "rock"
    },
    {
        id: 2,
        move: "rock",
        kills: "scissors"
    },
    {
        id: 3,
        move: "scissors",
        kills: "paper"
    }
];

describe('appReducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_SCORES_SUCCESS', () => {
        const response = scores;
        const finalState = { ...initialState, scores: response };

        expect(
            reducer(initialState, {
                type: types.GET_SCORES_SUCCESS,
                response
            })
        ).toEqual(finalState);
    });

    it('should handle GET_MOVES_SUCCESS', () => {
        const response = moves;
        const finalState = { ...initialState, moves: response };

        expect(
            reducer(initialState, {
                type: types.GET_MOVES_SUCCESS,
                response
            })
        ).toEqual(finalState);
    });

    it('should handle POST_MOVE_SUCCESS', () => {
        const response = {
            id: 8,
            move: "TES2T",
            kills: "rock"
        }
        const finalState = { ...initialState, moves: [response] };

        expect(
            reducer(initialState, {
                type: types.POST_MOVE_SUCCESS,
                response
            })
        ).toEqual(finalState);
    });

    it('should handle DELETE_MOVE_SUCCESS', () => {
        const response = {moveId: 1}
        const newInitial = { ...initialState, moves };
        const finalMoves = moves.filter(move => move.id !== 1) 
        const finalState = { ...newInitial, moves: finalMoves };
        expect(
            reducer(newInitial, {
                type: types.DELETE_MOVE_SUCCESS,
                response
            })
        ).toEqual(finalState);
    });
});
