import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import welcome from '../components/welcome';
import scoreboard from '../components/scoreboard';
import MovesSettings from '../components/move-settings';
import GameNames from '../components/game-names';

Enzyme.configure({ adapter: new Adapter() });
jest.useFakeTimers();

const moves = [
    {
        id: 1,
        move: "paper",
        kills: "rock"
    },
    {
        id: 2,
        move: "rock",
        kills: "scissors"
    },
    {
        id: 3,
        move: "scissors",
        kills: "paper"
    }
];

describe('welcome Component', () => {
    const initialState = {
    };
    it('it renders', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<welcome {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});

describe('scoreboard Component', () => {
    const initialState = {
    };
    it('it renders', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<scoreboard {...props} />);
        expect(wrapper).toMatchSnapshot();
    });
});

describe('MovesSettings Component', () => {
    const initialState = {
    };
    it('it renders', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<MovesSettings {...props} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('it playGame is called', () => {
        const props = {
            ...initialState,
            moves,
            postMove: jest.fn(),
            deleteMove: jest.fn(),
            playGame: jest.fn()
        };
        const wrapper = mount(<MovesSettings {...props} />);

        const button = wrapper.find('#playGameBtn').first();
        button.simulate('click');
        expect(props.playGame).toBeCalled();
    });
});

describe('GameNames Component', () => {
    const initialState = {
        continue: jest.fn()
    };
    it('it renders', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<GameNames {...props} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('it does not call continue if ames not set', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<GameNames {...props} />);
        jest.runTimersToTime(5000);
        const button = wrapper.find('#continue').first();
        button.simulate('click');
        expect(props.continue).not.toBeCalled();
    });

    it('it changes calls continue action if names set', () => {
        const props = {
            ...initialState
        };
        const wrapper = mount(<GameNames {...props} />);
        wrapper.setState({
            player1: 'test',
            player2: 'test2'
        });
        jest.runTimersToTime(5000);
        const button = wrapper.find('#continue').first();
        button.simulate('click');
        expect(props.continue).toBeCalled();
    });
});
